<?php
    
    function addToCart($db, $productId, $userId)
    {
        $stmt = $db->prepare("INSERT INTO `cart`(`product_id`, `user_id`, `quantity`) VALUES(:product_id, :user_id, 1)");
        $stmt->execute(["product_id" => $productId, "user_id" => $userId]);
        return;
    }
?>