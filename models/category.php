<?php 
    function getCategories($db)
    {
        $stmt = $db->query("SELECT * FROM categories");
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getUsedCategories($db)
    {
        $stmt = $db->query("SELECT DISTINCT `category_id` FROM products");
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function fetchCategoryTreeList($db, $parent = 0, $userTreeArray = '') {
        if (!is_array($userTreeArray)){
            $userTreeArray = [];
        }
        $sql = "SELECT * FROM `categories` WHERE `parent_id` = $parent ORDER BY id ASC";
        $stmt = $db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC); 
        if (count($result) > 0)
        {
            $userTreeArray[] = "<ul>";
            foreach($result as $row){
                $userTreeArray[] =  ((in_array($row["id"], array_column(getUsedCategories($db), 'category_id'))) 
                    ? ("<li><a href='/index.php?category=".$row['id']."'>". $row["title"]."</a></li>") 
                    : ("<li>".$row["title"]."</li>"));
                $userTreeArray = fetchCategoryTreeList($db, $row["id"], $userTreeArray);
            }
            $userTreeArray[] = "</ul><br/>";
        }
        return $userTreeArray;
    }

    function rutime($ru, $rus, $index) {
        return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
         -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
    }


?>