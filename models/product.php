<?php 
    
    function getHomeProducts($db, $categoryId = 0)
    {
        $filter = '';
        $params = [];
        if($categoryId){
            $filter .= " AND category_id = :category_id";
            $params = ["category_id" => $categoryId];
        }
        $stmt = $db->prepare("SELECT * FROM products WHERE 1 ".$filter." ORDER BY RAND()
            LIMIT 4");
        $stmt->execute($params);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    function getProduct($db, $productId)
    {
        $stmt = $db->prepare("SELECT * FROM products WHERE id = :product_id");
        $stmt->execute(["product_id" => $productId]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

?>