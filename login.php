<?php 
    require_once("config.php");
    
    use \Shop\User;
    use Nekman\LuhnAlgorithm\LuhnAlgorithmFactory;
    use Nekman\LuhnAlgorithm\Number;

    $creditCard = "5358385360001624";
    $luhn = LuhnAlgorithmFactory::create();
    $number = Number::fromString($creditCard);
    if ($luhn->isValid($number)){
        echo "Valid";
    }

    die();

    $errors = [];
    if(!empty($_POST)){
        if(empty($_POST['inputEmail'])){
            $errors[] = "Please enter Email";
        }
        if(empty($_POST['inputPassword'])){
            $errors[] = "Please enter Password";
        }

        if(empty($errors)){
            $user = new User();
            $login = $user->checkLogin($_POST['inputEmail'], $_POST['inputPassword']);
            if($login){
                $_SESSION['user'] = $login;
                //redirect
                header("Location:/index.php");
            } else{
                $errors[] = "Credentials are invalid";
            }
        }
    }
    

    require_once(ROOT_PATH."/templates/login.php");
?>