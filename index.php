<?php 
    require_once("config.php");
    //require_once(ROOT_PATH."/class/Product.php");
    //require_once(ROOT_PATH."/class/Category.php");

    if($_SESSION["user"]["type"] === "admin"){
            header("Location: /admin.php");
            die();
    }

    $products = new Product();
    $categoryId = $_GET['category'] ?? 0;
    $categoryId = (int) $categoryId;
    $products = $products->getHomeProducts($categoryId);
    $categories = Category::getCategories();
    $tree = Category::fetchCategoryTreeList();
    $category = new Category();

    require_once(ROOT_PATH."/templates/home.php");
?>