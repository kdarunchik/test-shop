<?php 
    define("ROOT_PATH", dirname(__FILE__));
    define("SITE_URL", "http://shop.loc");
    define("DB_USER", "root");
    define("DB_PASS", "root");
    define("DB_NAME", "test_db");
    session_start();

    function my_autoloader($className){
        $className = ltrim($className, '\\');
        $fileName = '';
        $namespace = '';
        if ($lastNsPos = strripos($className, '\\')){
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace).DIRECTORY_SEPARATOR;
        }
        $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
        require $fileName;
    }

    spl_autoload_register('my_autoloader');

   // require_once(ROOT_PATH."/class/DB.php");
    \Shop\DB::connect(DB_NAME, DB_USER, DB_PASS);
?>