<div class="card mb-4">
    <div><a href="<?php echo SITE_URL;?>/product_details.php?id=<?php echo $product['id'];?>">
      <img class="card-img-top" src="http://wx.somassbu.org/products/clouds/box.png"></div>
    <div class="card-body">
      <p class="card-text"><?php echo $product['title'];?></p>
      <div class="d-flex justify-content-between align-items-center">
        <div class="btn-group">
            <form method="POST" action="<?php echo SITE_URL. '/cart.php';?>">
                <input type="hidden" name="product_id" value="<?php echo $product['id'];?>">
                <button type="submit" class="btn btn-sm btn-outline-secondary">Order</button>
            </form>
        </div>
        <small class="text-muted"><?php echo $product['price']?></small>
      </div>
    </div>
</div>