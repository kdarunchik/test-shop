<?php
    require_once("config.php");
    require_once(ROOT_PATH."/models/product.php");
    require_once(ROOT_PATH."/models/category.php");

    $productId = $_GET['product_id'] ?? 0;
    $productId = (int) $productId;
    if(!$productId){
        header("Location: /index.php");
        die();
    }
    $product = getProduct($pdo, $productId);
    if(empty($product)){
        header("Location: /index.php");
        die();
    }

    require_once(ROOT_PATH."/templates/product_details.php");
?>