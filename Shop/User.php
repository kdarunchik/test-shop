<?php
namespace Shop;
use \Shop\AbstractClass\AbstractUser;
use \Shop\DB;

class User extends AbstractUser{
    protected const SALT = "kjdf756zkvhcu4";

    public function __construct()
    {
    }

    public function save()
    {
    }

    protected function encryptPass($pass)
    {
        $this->pass = sha1($pass.self::SALT);
        return $this->pass;
    }

    public function checkLogin($email, $pass)
    {
        $this->encryptPass($pass);
        $stmt = DB::$conn->prepare("SELECT * FROM users WHERE email = :email and password = :password");
        $stmt->execute(["email" => $email, "password" => $this->pass]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        if(!empty($result)){
            return ["id" => $result['id'], "type" => $result['type']];
        } else{
            return 0;
        }
    }

    
}