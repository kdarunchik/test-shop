<?php
namespace Shop\AbstractClass;

abstract class AbstractUser{
    public $id;
    public $name;
    public $email;
    protected $password;
    protected $type;
    
    abstract public function save();
    
    abstract protected function encryptPass($pass);
    
    abstract public function checkLogin($email, $pass);
    
    public static function logout()
    {
        session_destroy();
    }


}