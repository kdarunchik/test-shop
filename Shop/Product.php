<?php
namespace Shop;
use \Shop\DB;

class Product{
    public $id;
    public $title;
    public $price;
    public $quantity;
    public $categoryId;
    protected $createdDate;
    protected $updatedDate;
    private $db;

    public function __construct($id = null, $title = null, $price = null, $categoryId = null)
    {
        if(!empty($id)){
            $this->id = $id;
            $this->title = $title;
            $this->price = $price;
            $this->categoryId = $categoryId;
        }
    }

    public function zeroOutQuantity()
    {
        $this->quantity = 0;
    }

    public function setUpdatedDate($dateTime = null)
    {
        if(!isset($dateTime)){
            $dateTime = time();
        }
        $this->updatedDate = $dateTime;
    }

    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    public function getHomeProducts($categoryId = 0)
    {
        $filter = '';
        $params = [];
        $products = [];
        if($categoryId){
            $filter .= " AND category_id = :category_id";
            $params = ["category_id" => $categoryId];
        }
        $stmt = DB::$conn->prepare("SELECT * FROM products WHERE 1 ".$filter." ORDER BY RAND()
            LIMIT 4");
        $stmt->execute($params);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach($result as $product){
            $products[] = new Product($this->db, $product['id'], $product['title'], $product['price'], $product['category_id']);
        }
        return $result;
    }
}